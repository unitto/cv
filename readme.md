# CV as Code

Welcome to my CV as Code project!

I got tired of managing my resume in MS Word or Google Docs, so I decided to manage it in Markdown instead. This approach makes it much easier to maintain and keep track of changes.

## How It Works

I use [mkdocs](https://www.mkdocs.org/) and [Pandoc](https://pandoc.org/) to generate my CV in PDF format and publish it as a static website. This way, my CV is always up-to-date, easy to manage, and the entire process is automated using GitLab CI/CD pipelines.

## Why This Approach?

- **Simplicity**: Managing a resume in Markdown is straightforward and less cumbersome than traditional word processors.
- **Automation**: The process of generating and updating the PDF and HTML versions of my CV is fully automated.
- **Version Control**: Git provides a robust way to track changes and collaborate if needed.

## Get Started

If you like this project, feel free to star it. If you want to create your own CV as Code, fork this repository and start customizing it to fit your needs!
