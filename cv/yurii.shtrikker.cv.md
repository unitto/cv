# Yurii Shtrikker - Software Engineer / DevOps

## Contact information

Location - Kyiv, Ukraine | [Email] | [LinkedIn] | [WebSite] | [GitLab] | [GitHub]

[Email]: mailto:y2j01ah8y@mozmail.com?subject=Great_CV!
[LinkedIn]: https://www.linkedin.com/in/unitto/
[WebSite]: https://me.unittolabs.com/
[GitLab]: https://gitlab.com/unitto
[GitHub]: https://github.com/unittolabs

[//]: # (==================================================)
## Professional Summary

* Over 9 years of experience in development and operations across a variety of projects, including solo and team efforts with team sizes ranging from 5 to 15 members.
* Experience working with cross-functional teams, following Agile and Scrum methodologies.
* Proven track record of building systems from inception and maintaining them through maturity.
* Demonstrated technical leadership across five different companies, leading international teams with English as the primary language of communication.
* Strong focus on solving business problems effectively.
* Advocate for progress over perfection with a product-centric mindset.
* Evolved from a hobbyist to a professional developer, then to a DevOps engineer, and ultimately a technical leader.
* Extensive experience working in international teams, collaborating with professionals from diverse cultural backgrounds.

### Skills
* **Architecture & Design**: Monolithic Applications, Service-Oriented Architecture, Microservices, AWS Security Reference Architecture.
* **Programming**: Python (Django, Flask, FastAPI, SQLAlchemy, Celery, Pydantic, Click, Typer), Golang (Gin, Mux, GORM, Cobra).
* **Databases**: PostgreSQL, Stolon, MongoDB, Elasticsearch, MinIO, Vault, Consul.
* **Message Brokers**: RabbitMQ, Kafka, SQS.
* **API Development**: REST API, GraphQL, AMQP, WebSockets.
* **DevOps & Cloud**: Linux, Docker, Kubernetes, Kubernetes Operators, Helm, Terraform, Ansible, Istio, Azure, AWS, GCP, DigitalOcean.
* **Testing & CICD**: Locust, k6, Selenium, GitLab CI, Azure DevOps, GitHub Workflows, ArgoCD.
* **Monitoring & Observability**: Grafana, Loki, Mimir, Tempo, Elasticsearch, Logstash/Fluentd, Kibana, Datadog, ES Cloud.

[//]: # (==================================================)
## Professional Experience

### Software Engineer at [Qubex](https://www.qubex.ai/) - Dec 2023 - Jun 2024
Qubex provides a comprehensive Kubernetes scaling solution designed to ensure resiliency against volatile traffic without over-provisioning. Their platform offers tools such as QSim for simulating scale events, QScaler for managing hibernated nodes to handle intense scaling events, and QCache for preloading container images to optimize load times. The company's mission is to eliminate concerns about scale events and costs, making cloud operations more efficient and reliable.

#### Achievements
* Led an initiative to enhance code quality and maintainability.
* Designed and implemented mission-critical features and improvements, including auto-updates for agents in customer environments, image size calculation jobs, and an agent CLI.
* Made business and technical metrics visible and observable.
* Replaced external product dependencies by implementing required functionality in-house.

> **Technical stack:** Golang, AWS SDK, Ginkgo, Gin, Cobra, Controller Runtime, GitHub Actions, Docker, Kubernetes, AWS, MongoDB

[//]: # (=====)
### Technical Lead at [Covantis](https://covantis.io/) - Nov 2021 - Feb 2024
Covantis is a collaborative initiative in the commodities trading industry, aiming to replace outdated, inefficient post-trade processes with modern, secure digital solutions. Their platform is designed to minimize operational risks and increase market efficiency by streamlining the trade lifecycle from contract to settlement, thus enhancing transparency and security in global trade operations.

#### Achievements
* Enhanced security and delivery speed by refactoring existing CI pipelines.
* Implemented a new secrets engine, integrating it with CI/CD processes and workloads.
* Completely rebuilt the continuous integration process to improve time-to-market for features.
* Designed and built infrastructure architecture.
* Optimized, refactored, and improved reusability and maintainability of the existing Terraform codebase.

> **Technical stack:** Python, JavaScript / TypeScript, Terraform, Azure, AWS, Gitlab CI, Vault, ArgoCD, PostgreSQL, Docker, Kubernetes, Helm

[//]: # (=====)
### DevOps Engineer at [OTP Bank](https://en.otpbank.com.ua/) - Oct 2020 - Nov 2021
OTP Group is a leading financial service provider in Hungary and a significant player in the Central and Eastern European (CEE) banking market. The group operates in 10 countries besides Hungary, offering a wide range of financial services, including banking, leasing, and investment management. OTP Bank is known for its robust digital infrastructure and commitment to innovation in financial technology.

#### Achievements
* Designed and built cloud infrastructure architecture.
* Provided consultation for on-premise infrastructure.
* Implemented security-critical features and tools, including a secrets store and service mesh.
* Optimized and supported Kubernetes clusters for cost and performance efficiency.
* Enhanced existing CI processes.
* Developed a continuous delivery process.
* Established monitoring and application performance management (APM) systems.

> **Technical stack:** Java, Golang, Python, PostgreSQL, Kafka, Vault, Consul, Docker, Kubernetes, HELM, Azure, Azure Pipelines, Elasticsearch, Grafana, Loki, Cortex, Promtail, Prometheus, Jaeger, Istio

[//]: # (=====)
### Application Architect at [Ideal Society (ex DreamTek)](https://ideal-society.org) - Sep 2020 - Apr 2021
Ideal Society, formerly known as DreamTek, develops innovative social networking and dating applications like Fiorry and Elizium. Fiorry is the world's most popular social network and dating app for transgender people and their allies, while Elizium calculates compatibility between users based on life goals, lifestyle, values, personality, and habits to help them find ideal partners.

#### Achievements
* Reviewed and optimized solution architecture to improve availability and enable horizontal scaling.
* Designed and built cloud infrastructure architecture.
* Streamlined existing development processes.
* Implemented high availability, continuous integration, and continuous delivery practices.
* Optimized event bus and WebSocket handling for better performance.

> **Technical stack:** Golang, Python, PostgreSQL, RabbitMQ, Terraform, Docker, Kubernetes, HELM, Gitlab CI, Datadog, REST API, AMQP, WebSockets, GCP

[//]: # (=====)
### Technical Lead at [proSapient](https://www.prosapient.com) - Jul 2018 - Apr 2020
proSapient is a knowledge management platform that facilitates better decision-making by organizing consultations with industry experts and conducting extensive consumer surveys. Their software supports global research and expert networking, enabling businesses to access valuable insights and data-driven recommendations.

#### Achievements
* Established and optimized development processes.
* Designed and developed internal software architecture and infrastructure.
* Implemented high availability for the system.
* Developed and deployed a comprehensive monitoring system.
* Led the effort to transition from a monolithic architecture to microservices.
* Implemented continuous integration and continuous delivery practices.
* Provided technical leadership for the core solution and the team.

> **Technical stack:** Python 3, FastAPI, Django, Celery, PostgreSQL, Stolon, MongoDB, Elasticsearch, Vault, Consul, RabbitMQ, Terraform, Docker, Kubernetes, HELM, Gitlab CI, Datadog, REST API, ODATA, GraphQL, AMQP, GCP, AWS

[//]: # (=====)
### Technical Lead at [Kitway (ex Kit XXI)](https://kitway.com.ua) - Feb 2018 - Jul 2018
Kitway is an R&D vendor specializing in the automation of financial and audit operations. Known for its rapid prototyping capabilities, Kitway leverages extensive expertise to deliver efficient and innovative solutions for financial departments and companies. Their services include developing ERP solutions and business intelligence tools to enhance operational efficiency.

#### Achievements
* Provided leadership for the internal team.
* Successfully migrated the existing Odoo-based solution from Python 2 to Python 3 (Odoo 8 to Odoo 11).
* Developed an external testing framework for Odoo.

> **Technical stack:** Python, Odoo, Django, Celery, PostgreSQL, RabbitMQ, Docker, XMLRPC, REST API

[//]: # (=====)
### Software Engineer at [Kitway (ex Kit XXI)](https://kitway.com.ua) - Mar 2017 - Jul 2018
My 1st Years is a UK-based e-commerce startup that specializes in personalized gifts for babies and children. Founded by two best friends who created their first product, personalized baby shoes, My 1st Years has grown to offer a wide range of high-quality, personalized baby products. Known for its luxury gift packaging and next-day delivery, My 1st Years is a popular choice for parents looking for unique and memorable gifts for their children.

#### Achievements
* Enhanced the onboarding process for new team members.
* Developed new integrations for external vendors.
* Refactored and optimized mission-critical parts of the source code.

> **Technical stack:** Python, Odoo, Django, Celery, PostgreSQL, RabbitMQ, Docker, XMLRPC, REST API

[//]: # (=====)
### Technical Lead at [Bionorica SE](https://bionorica.com) - May 2016 - Oct 2016
Bionorica SE is one of the world's leading manufacturers of herbal remedies. Combining modern pharmaceutical research with the effectiveness of plants, Bionorica has been producing highly effective and well-tolerated phytopharmaceuticals for over 85 years. Their commitment to plant-based medicine makes them a pioneer in the field of herbal remedies.

#### Achievements
* Provided technical leadership for the team.
* Developed efficient ETL processes.
* Managed the implementation and optimization of BI solutions.

> **Technical stack:** Python, JavaScript, SQL, Odoo, PostgreSQL, Pentaho BI, Pentaho ETL, Hetzner

[//]: # (=====)
### DevOps Engineer at [Bionorica SE](https://bionorica.com) - May 2015 - Apr 2016
Bionorica is one of the world's leading manufacturers of herbal remedies, combining modern pharmaceutical research with the effectiveness of plants. For over 85 years, Bionorica has been producing highly effective and well-tolerated phytopharmaceuticals.

#### Achievements
* Developed new add-ons for the ERP solution.
* Created and maintained BI dashboards.
* Supported and managed cloud VMs.
* Provided maintenance and support for internal machines and servers.

> **Technical stack:** Python, JavaScript, SQL, Odoo, PostgreSQL, Pentaho BI, Pentaho ETL, Hetzner

[//]: # (==================================================)
## Education
### National Technical University of Ukraine 'Kyiv Polytechnic Institute' - 2009 - 2011
Pursued a Bachelor's degree in Computer Systems and Devices. Although the degree was not completed, I have leveraged my academic foundation to achieve significant success in my professional career.

## References
I have many recommendations from my former and current colleagues on [my LinkedIn](https://www.linkedin.com/in/unitto/) profile. Feel free to [check them out](https://www.linkedin.com/in/unitto/details/recommendations/).

## Languages
* English - Advanced
* Ukrainian - native
* Russian - fluent
