# What is it all about?

Hi there, _Anonymous_! 🕵️‍♂️

You'll probably laugh 😂, but this is my second attempt at experimenting with the CV as code approach.

[//]: # (> **NOTE:** You can find the PDF export button on each of the pages to the right side of the page title. Just click it to download the article as a PDF.)

## I'm an engineer who loves to automate routine tasks

I found the Experience page on [LinkedIn](https://www.linkedin.com/in/unitto/) lacking in user experience and quite challenging to maintain without support for markup languages like Markdown.

So, I decided to write my resume in [Markdown](https://www.markdownguide.org/) and generate the final PDF from it.

To eliminate manual work, I started using [GitLab](https://gitlab.com/) by placing my resume there and utilizing pipelines and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) for automation. Now, all I need to do is update my CV, push it to the main branch, and let CI take care of the rest 😎.

## A few words about the first iteration

In the first iteration, I used [Pandoc](https://pandoc.org/), running it twice: once for HTML generation and then again for PDF conversion.

It worked, BUT after a year, I still hadn't found the time to add CSS styles to my HTML page, nor did I manage to create a proper frontend (I'm not a fan of front-end work). So, this time I decided to use mkdocs to avoid such issues 😅.

> **NOTE:** The first iteration is still available in the git history, so feel free to check it out if you're interested.

## Afterword

You're currently reading the index page, but there are two more pages: one with my general introduction and summary, and another with the CV without additional information.

Feel free to check them out if you're interested in my personality or if you're looking for a skilled engineer to solve your business problems 😎.
