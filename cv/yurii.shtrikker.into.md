# Long story short

This article is my short story that can help you understand what kind of engineer I am.
Feel free to read it briefly or skip it for a detailed read of my CV on the next page.

## Foreword

Hey there! My name is Yurii, and I'm a 32-year-old Ukrainian currently based in Kyiv. 👋

I discovered my passion for programming back in school. At 16, I taught myself [PHP](https://www.php.net/) and [MySQL](https://www.mysql.com/) for backend development, along with [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML), [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS), and [jQuery](https://jquery.com/) for frontend. I even figured out how to deploy websites on virtual machines all by myself. 💻

During university, I focused on C++, but eventually, [Python](https://www.python.org/) became my primary language. Nowadays, I also enjoy working with [Golang](https://golang.org/), and I have a soft spot for Rust, even though I'm not entirely comfortable with it yet. 🐍 💖 🦀

At the start of my career, I worked with ERP systems, holding various positions from support manager to technical support and system administration. Programming was just a hobby during this time. 💼

In 2015, I officially entered the IT industry as a mid-level software engineer. By then, I had been coding in Python for three years and had a few personal projects under my belt. I never confined myself to just backend or frontend roles; instead, I was always interested in the entire development cycle. I saw myself as a technical lead or architect, someone who could tackle business problems with technology. ⚙️

Almost every company I worked for eventually saw me transition into a tech lead role. 🚀

Today, in 2024, I'm the Technical Leader of a DevOps team in a European company. A funny thing happened three years ago when I joined a company and, after passing all the interviews, was surprised to be offered a Senior DevOps position. Since then, I've mostly focused on DevOps professionally, but I still love coding in both my work and personal projects. 🚀

## Who am I now, in 2024?

I'm a technical expert and an engineer with 8 years of industry experience in various roles:
- Software Engineer
- Dev(Sec)Ops Engineer
- Technical Lead
- Application Architect

I have commercial experience in Python and Golang, and I still code in my roles as a DevOps engineer and tech lead. 💻

Usually, I work closely with the business to understand its problems and gaps, solving them technically, either by doing it myself or creating roadmaps and delegating tasks to the team. 📈

> ... and I still love my job as if it were a hobby. I never lose the desire to learn and grow.
> For example, my latest pet project is a Kubernetes operator for automating load testing applications in Kubernetes environments.
> I'm also involved in a volunteer project for the government, helping with the recovery of our country during times of war. 💪 🇺🇦

## What am I looking for?

Amidst the War, I've realized it takes a lot of my energy and creates mental overload. Therefore, I'm looking for a company that offers a good work-life balance, flexible working hours, and an environment with reasonable pressure and stress. I want to focus more on engineering work (Software Engineer, DevOps) rather than spending all day in meetings. ⚖️ 🕒

After the War, I'd be happy to evolve into a Technical Lead or even an Architect (Application of Solution). I'm also open to business trips or relocation opportunities. Having enough flexibility in my work would also allow me to continue my travels around the world. 🌍 ✈️

[Let's connect and discuss how we can work together to solve your business problems! 🚀](https://www.linkedin.com/in/unitto/)
